var urlParams = new URLSearchParams(window.location.search);
var current
var grid
var suff=""

$(document).ready(function() {
    $('#toggle').change(function() {
      if ($(this).prop('checked')) {
        update(current.stats);
        suff="";
      } else {
        update(current.ratio);
        suff="%";
      }
//      console.log($(this).prop('checked'));
    })



  if (urlParams.has('data')) {
    $.getJSON(urlParams.get('data') + ".json")
      .done(function(data) {
        current = data;
        fill_table(data.patterns, data.stats);
      })
      .fail(function() {
        $("#wrapper").html(`<h1>Fail to load "${urlParams.get('data')}.json"<h1>`);
      })
  } else {
    $("#wrapper").html("<h1>This web page is expected a `data` GET argument!<h1>");
  }
})

function update(stats) {
  grid.updateConfig({
    search: true,
    data: stats
  }).forceRender();
  post_grid();
}

function fill_table(patterns, stats) {
  grid = new gridjs.Grid({
    columns: ["Corpus"].concat(Object.keys(patterns).map(build_col)),
    search: true,
    sort: true,
    fixedHeader: true,
    data: stats
  }).render(document.getElementById("wrapper"));
  post_grid();
}

function post_grid() {
  $(".gridjs-input").attr("placeholder", "Search");
}

function build_col(name) {
  return ({
    name: name,
    formatter: (cell, row) =>
      gridjs.html(`<a class="btn btn-success" onclick='grew_match("${row.cells[0].data}","${name}")'>${cell}${suff}</a>`)
  })
}

function grew_match(v, n) {
  console.log("===" + v + "***" + n + "===");
  pattern_lines = current.patterns[n]["code"];
  pattern = pattern_lines.join("\n");
  console.log(pattern);
  let get_param = "?corpus=" + v;
  get_param += "&pattern=" + encodeURIComponent(pattern);
  window.open('http://match.grew.fr' + get_param, '_blank');
}